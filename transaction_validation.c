#include "transaction_validation.h"

int sign_transaction(User *from_user, const char *to_name, double amount, unsigned char **sig, size_t *sig_len) {
    char message[100];
    snprintf(message, sizeof(message), "%s:%s:%.2f", from_user->name, to_name, amount);

    EVP_MD_CTX *mdctx = EVP_MD_CTX_new();
    if (mdctx == NULL) {
        printf("Error creating message digest context.\n");
        return 0;
    }

    if (EVP_DigestSignInit(mdctx, NULL, EVP_sha256(), NULL, from_user->keypair) <= 0) {
        printf("Error initializing digest sign.\n");
        EVP_MD_CTX_free(mdctx);
        return 0;
    }

    if (EVP_DigestSignUpdate(mdctx, message, strlen(message)) <= 0) {
        printf("Error updating digest sign.\n");
        EVP_MD_CTX_free(mdctx);
        return 0;
    }

    if (EVP_DigestSignFinal(mdctx, NULL, sig_len) <= 0) {
        printf("Error finalizing digest sign (getting length).\n");
        EVP_MD_CTX_free(mdctx);
        return 0;
    }

    *sig = (unsigned char *)malloc(*sig_len);
    if (*sig == NULL) {
        printf("Error allocating memory for signature.\n");
        EVP_MD_CTX_free(mdctx);
        return 0;
    }

    if (EVP_DigestSignFinal(mdctx, *sig, sig_len) <= 0) {
        printf("Error finalizing digest sign.\n");
        free(*sig);
        EVP_MD_CTX_free(mdctx);
        return 0;
    }

    EVP_MD_CTX_free(mdctx);
    return 1;
}

int verify_transaction(User *from_user, const char *to_name, double amount, unsigned char *sig, size_t sig_len) {
    char message[100];
    snprintf(message, sizeof(message), "%s:%s:%.2f", from_user->name, to_name, amount);

    EVP_MD_CTX *mdctx = EVP_MD_CTX_new();
    if (mdctx == NULL) {
        printf("Error creating message digest context.\n");
        return 0;
    }

    if (EVP_DigestVerifyInit(mdctx, NULL, EVP_sha256(), NULL, from_user->keypair) <= 0) {
        printf("Error initializing digest verify.\n");
        EVP_MD_CTX_free(mdctx);
        return 0;
    }

    if (EVP_DigestVerifyUpdate(mdctx, message, strlen(message)) <= 0) {
        printf("Error updating digest verify.\n");
        EVP_MD_CTX_free(mdctx);
        return 0;
    }

    int verify_status = EVP_DigestVerifyFinal(mdctx, sig, sig_len);
    EVP_MD_CTX_free(mdctx);

    if (verify_status != 1) {
        printf("Error verifying the transaction: %s\n", ERR_error_string(ERR_get_error(), NULL));
        return 0;
    }

    return 1;
}

void perform_transaction(const char *from_name, const char *to_name, double amount) {
    User *from_user = find_user(from_name);
    User *to_user = find_user(to_name);

    if (from_user == NULL || to_user == NULL) {
        printf("User not found.\n");
        return;
    }

    if (from_user->balance < amount) {
        printf("Insufficient funds.\n");
        return;
    }

    unsigned char *sig;
    size_t sig_len;
    if (!sign_transaction(from_user, to_name, amount, &sig, &sig_len)) {
        printf("Transaction signing failed.\n");
        return;
    }

    if (!verify_transaction(from_user, to_name, amount, sig, sig_len)) {
        printf("Transaction verification failed.\n");
        free(sig);
        return;
    }

    from_user->balance -= amount;
    to_user->balance += amount;

    printf("Transaction complete: %s -> %s : %.2f\n", from_name, to_name, amount);
    free(sig);
}