// aurum.h

#ifndef AURUM_H
#define AURUM_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <pthread.h>
#include <openssl/rsa.h>
#include <openssl/pem.h>
#include <openssl/err.h>
#include <openssl/evp.h>

#include "init.h"

// Include les fichiers d'en-tête pour la validation des transactions, la gestion des clés privées et la sécurité réseau
#include "miner.h" // Include miner functions
#include "transaction_validation.h"
#include "private_key_management.h"
#include "network_security.h"

#endif /* AURUM_H */
