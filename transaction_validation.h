// transaction_validation.h

#ifndef TRANSACTION_VALIDATION_H
#define TRANSACTION_VALIDATION_H

#include <openssl/rsa.h>
#include <openssl/pem.h>
#include <openssl/evp.h>

#include "init.h"

int sign_transaction(User *from_user, const char *to_name, double amount, unsigned char **sig, size_t *sig_len);

int verify_transaction(User *from_user, const char *to_name, double amount, unsigned char *sig, size_t sig_len);

void perform_transaction(const char *from_name, const char *to_name, double amount);

#endif /* TRANSACTION_VALIDATION_H */
