// miner.h

#ifndef MINER_H
#define MINER_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <openssl/rsa.h>
#include <openssl/pem.h>
#include <openssl/err.h>
#include <openssl/evp.h>
#include <openssl/rand.h>

#include "init.h"

#define DIFFICULTY 3

int mine_block(const char *block_data, unsigned char *nonce, size_t *nonce_len);

#endif /* MINER_H */
