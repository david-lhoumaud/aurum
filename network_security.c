#include "network_security.h"

// Network-related functions
void *handle_connection(void *client_socket) {
    int socket = *(int *)client_socket;
    free(client_socket);

    char buffer[1024];
    int bytes_received = recv(socket, buffer, sizeof(buffer) - 1, 0);
    if (bytes_received < 0) {
        perror("recv");
        close(socket);
        return NULL;
    }
    buffer[bytes_received] = '\0';

    // Parsing the received transaction
    char from_name[MAX_NAME_LENGTH], to_name[MAX_NAME_LENGTH];
    double amount;
    unsigned char sig[256];
    size_t sig_len;

    sscanf(buffer, "%s %s %lf %s", from_name, to_name, &amount, sig);

    sig_len = strlen((char *)sig);

    // Verify and perform the transaction
    User *from_user = find_user(from_name);
    if (from_user == NULL) {
        printf("User not found: %s\n", from_name);
        close(socket);
        return NULL;
    }

    if (!verify_transaction(from_user, to_name, amount, sig, sig_len)) {
        printf("Transaction verification failed.\n");
        close(socket);
        return NULL;
    }

    perform_transaction(from_name, to_name, amount);
    close(socket);
    return NULL;
}

void start_server() {
    int server_fd, client_socket;
    struct sockaddr_in address;
    int opt = 1;
    int addrlen = sizeof(address);

    // Creating socket file descriptor
    if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0) {
        perror("socket failed");
        exit(EXIT_FAILURE);
    }

    // Forcefully attaching socket to the port 8080
    if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt))) {
        perror("setsockopt");
        exit(EXIT_FAILURE);
    }
    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons(PORT);

    // Binding the socket to the network address and port
    if (bind(server_fd, (struct sockaddr *)&address, sizeof(address)) < 0) {
        perror("bind failed");
        exit(EXIT_FAILURE);
    }
    if (listen(server_fd, 3) < 0) {
        perror("listen");
        exit(EXIT_FAILURE);
    }

    printf("Server started on port %d\n", PORT);

    while ((client_socket = accept(server_fd, (struct sockaddr *)&address, (socklen_t *)&addrlen)) >= 0) {
        printf("New connection accepted\n");

        pthread_t thread_id;
        int *client_socket_ptr = malloc(sizeof(int));
        *client_socket_ptr = client_socket;

        if (pthread_create(&thread_id, NULL, handle_connection, client_socket_ptr) != 0) {
            perror("pthread_create");
            close(client_socket);
            free(client_socket_ptr);
        }
    }

    if (client_socket < 0) {
        perror("accept");
        exit(EXIT_FAILURE);
    }
}

void send_transaction(const char *server_ip, const char *from_name, const char *to_name, double amount) {
    int sock = 0;
    struct sockaddr_in serv_addr;
    char buffer[1024] = {0};
    unsigned char *sig;
    size_t sig_len;

    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        printf("\n Socket creation error \n");
        return;
    }

    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(PORT);

    if (inet_pton(AF_INET, server_ip, &serv_addr.sin_addr) <= 0) {
        printf("\nInvalid address/ Address not supported \n");
        return;
    }

    if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0) {
        printf("\nConnection Failed \n");
        return;
    }

    User *from_user = find_user(from_name);
    if (from_user == NULL) {
        printf("User not found: %s\n", from_name);
        return;
    }

    if (!sign_transaction(from_user, to_name, amount, &sig, &sig_len)) {
        printf("Transaction signing failed.\n");
        return;
    }

    snprintf(buffer, sizeof(buffer), "%s %s %.2f %s", from_name, to_name, amount, sig);
    send(sock, buffer, strlen(buffer), 0);

    free(sig);
    close(sock);
}