# Aurum

Aurum est un prototype de monnaie numérique décentralisée simple implémenté en C. Il démontre les fonctionnalités de base pour créer des utilisateurs, effectuer des transactions et vérifier des transactions à l'aide de signatures RSA sur un réseau.

## Prérequis

Avant de compiler et d'exécuter Aurum, assurez-vous d'avoir les bibliothèques et les outils suivants installés :

- Bibliothèque OpenSSL (`libssl-dev`)
- Threads POSIX (`pthread`)

Sur un système basé sur Debian, vous pouvez installer les bibliothèques nécessaires à l'aide de la commande suivante :

```sh
sudo apt-get install libssl-dev
```

## Compilation

Pour compiler Aurum, naviguez jusqu'au répertoire contenant `aurum.c` et `Makefile`, puis exécutez :

```sh
make
```

Cela produira un exécutable nommé `aurum`.

## Utilisation

Aurum peut fonctionner en mode serveur ou en mode client. Voici les commandes pour démarrer le serveur et envoyer des transactions à partir d'un client.

### Démarrer le serveur

Pour démarrer le serveur, exécutez :

```sh
./aurum server
```

Le serveur écoutera sur le port 8080 pour les connexions entrantes.

### Envoyer une transaction

Pour envoyer une transaction à partir d'un client, exécutez :

```sh
./aurum client <server_ip> <from_name> <to_name> <amount>
```

Remplacez `<server_ip>` par l'adresse IP du serveur, `<from_name>` par le nom de l'utilisateur envoyant la transaction, `<to_name>` par le nom du destinataire et `<amount>` par le montant à transférer.

### Exemple

1. Démarrez le serveur sur un terminal :

```sh
./aurum server
```

2. Envoyez une transaction à partir d'un client sur un autre terminal :

```sh
./aurum client 127.0.0.1 Alice Bob 20.0
```

Dans cet exemple, le client envoie 20,0 unités d'Aurum d'Alice à Bob à un serveur en cours d'exécution sur `127.0.0.1`.

## Miner des blocs

Pour miner de nouveaux blocs dans le réseau Aurum, vous pouvez utiliser la fonctionnalité de minage en exécutant :

```sh
./aurum mine
```

Cette commande tente de résoudre un problème de preuve de travail pour créer un nouveau bloc dans la chaîne de blocs.

## Nettoyage

Pour nettoyer les fichiers compilés, exécutez :

```sh
make clean
```

Cela supprimera l'exécutable `aurum` et tout fichier objet généré lors de la compilation.

## Notes

- Cette implémentation est un prototype simplifié et n'est pas destinée à un usage de production.
- Pour une cryptomonnaie réelle, des fonctionnalités supplémentaires telles que le stockage sécurisé des clés, la communication chiffrée et des mécanismes de consensus sont nécessaires.

## Licence

Ce projet est open-source et sous licence MIT. Vous êtes libre de l'utiliser, de le modifier et de le distribuer conformément aux termes de la licence.