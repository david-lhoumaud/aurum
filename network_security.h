// network_security.h

#ifndef NETWORK_SECURITY_H
#define NETWORK_SECURITY_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <pthread.h>
#include <openssl/rsa.h>
#include <openssl/pem.h>
#include <openssl/err.h>
#include <openssl/evp.h>

#include "init.h"

void *handle_connection(void *client_socket);

void start_server();

void send_transaction(const char *server_ip, const char *from_name, const char *to_name, double amount);

#endif /* NETWORK_SECURITY_H */
