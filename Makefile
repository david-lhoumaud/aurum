# Makefile for Aurum cryptocurrency project

CC = gcc
CFLAGS = -Wall -Wextra -lssl -lcrypto -pthread
TARGET = aurum

# Add the header files and network_security.c to the dependencies
DEPS = aurum.h init.h miner.h transaction_validation.h private_key_management.h network_security.h
OBJ = aurum.o init.o miner.o transaction_validation.o private_key_management.o network_security.o

all: $(TARGET)

$(TARGET): $(OBJ)
	$(CC) -o $(TARGET) $(OBJ) $(CFLAGS)
	rm -f *.o

# Update the compilation rule to include all header files
%.o: %.c $(DEPS)
	$(CC) -c -o $@ $< $(CFLAGS)

clean:
	rm -f *.o $(TARGET)

.PHONY: all clean
