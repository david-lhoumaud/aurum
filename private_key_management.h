// private_key_management.h

#ifndef PRIVATE_KEY_MANAGEMENT_H
#define PRIVATE_KEY_MANAGEMENT_H

#include <openssl/rsa.h>
#include <openssl/pem.h>
#include <openssl/evp.h>

#include "init.h"

void create_user(const char *name, double initial_balance);

User* find_user(const char *name);

#endif /* PRIVATE_KEY_MANAGEMENT_H */
