#include "aurum.h" // Inclure le fichier d'en-tête principal

void display_users() {
    printf("Users:\n");
    for (int i = 0; i < user_count; i++) {
        printf("Name: %s, Balance: %.2f\n", users[i].name, users[i].balance);
    }
}

int main(int argc, char const *argv[]) {
    // Initialize OpenSSL
    OpenSSL_add_all_algorithms();
    ERR_load_crypto_strings();
    
    // Create sample users
    create_user("Alice", 100.0);
    create_user("Bob", 50.0);
    
    if (argc > 1 && strcmp(argv[1], "server") == 0) {
        start_server();
    } else if (argc > 4 && strcmp(argv[1], "client") == 0) {
        const char *server_ip = argv[2];
        const char *from_name = argv[3];
        const char *to_name = argv[4];
        double amount = atof(argv[5]);

        send_transaction(server_ip, from_name, to_name, amount);
    } else if (argc > 1 && strcmp(argv[1], "mine") == 0) {
        // Mining command
        unsigned char nonce[32]; // Nonce
        size_t nonce_len = sizeof(nonce);

        char block_data[100];
        snprintf(block_data, sizeof(block_data), "Block data: %s", "Sample transaction data");

        if (mine_block(block_data, nonce, &nonce_len)) {
            printf("Block mined successfully.\n");
        } else {
            printf("Mining failed.\n");
        }
    } else {
        printf("Usage:\n");
        printf("  Server: %s server\n", argv[0]);
        printf("  Client: %s client <server_ip> <from_name> <to_name> <amount>\n", argv[0]);
        printf("  Miner: %s mine\n", argv[0]);
    }
    
    // Cleanup EVP key pairs
    for (int i = 0; i < user_count; i++) {
        EVP_PKEY_free(users[i].keypair);
    }
    
    // Cleanup OpenSSL
    EVP_cleanup();
    ERR_free_strings();
    
    return 0;
}
