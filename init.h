#ifndef INIT_H
#define INIT_H

#include <openssl/evp.h> // Ajout de l'inclusion pour EVP_PKEY

#define MAX_USERS 10
#define MAX_NAME_LENGTH 50
#define PORT 8080

typedef struct {
    char name[MAX_NAME_LENGTH];
    double balance;
    EVP_PKEY *keypair;
} User;

extern User users[MAX_USERS]; // Déclaration externe
extern int user_count;

#endif /* INIT_H */
