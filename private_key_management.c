#include "private_key_management.h"

void create_user(const char *name, double initial_balance) {
    if (user_count >= MAX_USERS) {
        printf("Maximum number of users reached.\n");
        return;
    }
    strncpy(users[user_count].name, name, MAX_NAME_LENGTH);
    users[user_count].balance = initial_balance;

    // Generate RSA key pair
    EVP_PKEY_CTX *pkey_ctx = EVP_PKEY_CTX_new_id(EVP_PKEY_RSA, NULL);
    if (EVP_PKEY_keygen_init(pkey_ctx) <= 0 || EVP_PKEY_CTX_set_rsa_keygen_bits(pkey_ctx, 2048) <= 0) {
        printf("Error initializing RSA key generation.\n");
        EVP_PKEY_CTX_free(pkey_ctx);
        return;
    }

    if (EVP_PKEY_keygen(pkey_ctx, &users[user_count].keypair) <= 0) {
        printf("Error generating RSA key pair.\n");
        EVP_PKEY_CTX_free(pkey_ctx);
        return;
    }

    EVP_PKEY_CTX_free(pkey_ctx);
    user_count++;
}

User* find_user(const char *name) {
    for (int i = 0; i < user_count; i++) {
        if (strncmp(users[i].name, name, MAX_NAME_LENGTH) == 0) {
            return &users[i];
        }
    }
    return NULL;
}