#include "miner.h"

int mine_block(const char *block_data, unsigned char *nonce, size_t *nonce_len) {
    unsigned char hash[EVP_MAX_MD_SIZE];
    unsigned int hash_len;
    char hash_hex[2 * EVP_MAX_MD_SIZE + 1];

    EVP_MD_CTX *mdctx = EVP_MD_CTX_new();
    if (mdctx == NULL) {
        printf("Error creating message digest context.\n");
        return 0;
    }

    while (1) {
        if (!RAND_bytes(nonce, *nonce_len)) {
            printf("Error generating nonce.\n");
            EVP_MD_CTX_free(mdctx);
            return 0;
        }

        EVP_DigestInit(mdctx, EVP_sha256());
        EVP_DigestUpdate(mdctx, block_data, strlen(block_data));
        EVP_DigestUpdate(mdctx, nonce, *nonce_len);
        EVP_DigestFinal(mdctx, hash, &hash_len);

        int leading_zeroes = 0;
        for (size_t i = 0; i < hash_len && hash[i] == 0; i++) {
            leading_zeroes++;
        }

        if (leading_zeroes >= DIFFICULTY) {
            for (size_t i = 0; i < hash_len; i++) {
                sprintf(&hash_hex[i * 2], "%02x", hash[i]);
            }
            printf("Block mined: Nonce: %s\n", nonce);
            printf("Hash: %s\n", hash_hex);
            EVP_MD_CTX_free(mdctx);
            return 1;
        }
    }

    EVP_MD_CTX_free(mdctx);
    return 0;
}
